function init() {
    API.print('on init');
}

var goAtRightUnless = [
    [4, 7]
];

function shouldGoRight() {
    if(!API.isOnGround())
        return false;

    if(API.isMarioGoingLeft())
        return false;

    var result = true;
    var marioX = API.getMarioX();
    var marioY = API.getMarioY();

    goAtRightUnless.forEach(
        function(coord) {
            if(marioX >= coord[0] && marioX <= coord[1])
            {
                result = false;
            }
        }
    )

    return result;
}

function array_contains_value(array, val)
{
    var result = false;

    array.forEach(
        function(value) {
            if(value === val)
                result = true;
        }
    );

    return result;
}

var stabilizeCnt = 0;
var idarray = [1, 3, 5];
var ended = false;
function update() {
    API.print("State: " + stabilizeCnt);

    if(stabilizeCnt === 7)
    {
        API.setMarioGoRight(true);
        API.setMarioGoLeft(false);
        stabilizeCnt++;

        return;
    }

    if(stabilizeCnt === 8)
    {
        if (API.getMarioX() >= 5 && API.getMarioX() <= 6) {
            stabilizeCnt++;
            API.setMarioGoLeft(true);
            API.setMarioGoRight(false);
        }

        return;
    }

    if(stabilizeCnt === 9)
    {
        if(API.hasStabilizedPoint())
        {
            ended = true;
        }

        else if(ended) {
            stabilizeCnt++;
            API.stabilizeEnd();
            API.setMarioGoRight(true);
        }

        else if (API.getMarioX() >= 4 && API.getMarioX() <= 5) {
            API.stabilize();
        }

        return;
    }

    if(stabilizeCnt === 10)
    {
        API.setMarioGoRight(true);

        if (API.getMarioX() >= 6 && API.getMarioX() <= 7) {
            API.marioJump();
        }

        if (API.getMarioX() >= 12 && API.getMarioX() <= 13) {
            API.marioJump();
        }

        if (API.getMarioX() >= 17 && API.getMarioX() <= 18) {
            API.marioJump();
        }

        return;
    }

    if(shouldGoRight())
        API.setMarioGoRight(true);

    if (array_contains_value(idarray, stabilizeCnt) && !API.hasStabilizedPoint()) {
        API.marioJump();
        stabilizeCnt++;
        return;
    }

    if (API.getMarioX() >= 4 && API.getMarioX() <= 5) {
        if (stabilizeCnt === 0) {
            API.setMarioGoRight(false);
            stabilizeCnt++;
            API.stabilize();
        }

        if(stabilizeCnt === 2 && API.isOnGround()) {
            API.setMarioGoRight(true);
        }

    }

    else if (API.getMarioX() >= 5 && API.getMarioX() <= 6) {
        if (stabilizeCnt === 2) {
            API.setMarioGoRight(false);
            stabilizeCnt++;
            API.stabilize();
        }
    }

    else if (API.getMarioX() >= 6 && API.getMarioX() <= 7) {
        if (stabilizeCnt === 4) {
            API.setMarioGoRight(false);
            stabilizeCnt++;
            API.stabilize();
        }
    }

    if(stabilizeCnt === 6) {
        API.stabilizeEnd();
        API.setMarioGoLeft(true);
        API.setMarioGoRight(false);

        if(API.getMarioX() === 1)
        {
            API.marioJump();
            stabilizeCnt++;
        }
    }

    if(stabilizeCnt === 4) {
        API.setMarioGoRight(true);
    }
}

function dispose() {
    //API.print('on dispose');
}
