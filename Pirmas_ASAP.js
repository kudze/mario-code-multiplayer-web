function init() {
    API.print('on init');
}

function update() {
    API.setMarioGoRight(true);

    if (API.getMarioX() >= 6 && API.getMarioX() <= 7) {
        API.marioJump();
    }

    if (API.getMarioX() >= 12 && API.getMarioX() <= 13) {
        API.marioJump();
    }

    if (API.getMarioX() >= 17 && API.getMarioX() <= 18) {
        API.marioJump();
    }

    API.print(API.getMarioX() + " " + API.getMarioY());
}

function dispose() {
    //API.print('on dispose');
}
