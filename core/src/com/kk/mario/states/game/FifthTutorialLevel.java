package com.kk.mario.states.game;

import com.kk.mario.states.game.map.*;

public class FifthTutorialLevel extends LevelState {

    public FifthTutorialLevel() {
        super(
                new Map(20, 12),
                2, 1,
                "Fifth_Tutorial"
        );

        for (int i = 0; i < 20; i++) {
            this.getMap().setBlockAtXY(
                    i,
                    11,
                    new Stone(this.getBlocksTextureAtlas())
            );

            this.getMap().setBlockAtXY(
                    i,
                    0,
                    new Stone(this.getBlocksTextureAtlas())
            );
        }

        for (int i = 0; i < 12; i++) {
            this.getMap().setBlockAtXY(
                    0,
                    i,
                    new Stone(this.getBlocksTextureAtlas())
            );

            this.getMap().setBlockAtXY(
                    19,
                    i,
                    new Stone(this.getBlocksTextureAtlas())
            );
        }

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j <= i; j++) {
                this.getMap().setBlockAtXY(14 - i, j, new Brick(this.getBlocksTextureAtlas()));
            }
        }

        this.getMap().setBlockAtXY(7, 4, new Question(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(8, 4, new Question(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(9, 4, new Question(this.getBlocksTextureAtlas()));

        this.getMap().setBlockAtXY(17, 1, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 2, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 3, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 4, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 5, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 6, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 7, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(17, 8, new Flag(this.getBlocksTextureAtlas(), true));
    }

}
