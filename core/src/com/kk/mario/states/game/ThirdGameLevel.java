package com.kk.mario.states.game;

import com.kk.mario.states.game.entities.enemies.Mushroom;
import com.kk.mario.states.game.entities.enemies.Smile;
import com.kk.mario.states.game.entities.mario.Mario;
import com.kk.mario.states.game.map.*;

public class ThirdGameLevel extends LevelState {
    public ThirdGameLevel()
    {
        super(new Map(70, 40), 2, 8, "Trecias");

        for(int i = 0; i < 70; i++) {
            this.getMap().setBlockAtXY(
                    i,
                    39,
                    new Stone(this.getBlocksTextureAtlas())
            );

        }

        for(int i = 0; i < 40; i++) {
            this.getMap().setBlockAtXY(
                    0,
                    i,
                    new Stone(this.getBlocksTextureAtlas())
            );

            this.getMap().setBlockAtXY(
                    69,
                    i,
                    new Stone(this.getBlocksTextureAtlas())
            );
        }

        for(int i = 0; i < 5; i++) {
            for(int j = 0; j < 4; j++) {
                this.getMap().setBlockAtXY(i + 1, j, new Brick(this.getBlocksTextureAtlas()));
            }
        }

        for(int i = 0; i < 8; i++) {
            this.getMap().setBlockAtXY(7 + i, 9, new Brick(this.getBlocksTextureAtlas()));
        }

        for(int i = 0; i < 5; i++) {
            this.getMap().setBlockAtXY(10 + i, 4, new Brick(this.getBlocksTextureAtlas()));
        }

        for(int i = 0; i < 15; i++) {
            this.getMap().setBlockAtXY(15 + i, 0, new Brick(this.getBlocksTextureAtlas()));
        }

        for(int i = 0; i < 7; i++) {
            this.getMap().setBlockAtXY(19, i + 3, new Brick(this.getBlocksTextureAtlas()));
        }

        for(int i = 0; i < 4; i++) {
            this.getMap().setBlockAtXY(20 + i, 3, new Brick(this.getBlocksTextureAtlas()));
        }

        for(int i = 0; i < 20; i++) {
            this.getMap().setBlockAtXY(28, i, new Stone(this.getBlocksTextureAtlas()));
            this.getMap().setBlockAtXY(29, i, new Stone(this.getBlocksTextureAtlas()));
        }
        for(int i = 0; i < 30; i++) {
            if(i > 1 && i < 29) {
                this.getMap().setBlockAtXY(27 + i, 22, new Coin(this.getBlocksTextureAtlas()));
                this.getMap().setBlockAtXY(27 + i, 23, new Coin(this.getBlocksTextureAtlas()));
            }

            this.getMap().setBlockAtXY(28 + i, 20, new Stone(this.getBlocksTextureAtlas()));
            this.getMap().setBlockAtXY(28 + i, 19, new Stone(this.getBlocksTextureAtlas()));
            this.getMap().setBlockAtXY(28 + i, 18, new Stone(this.getBlocksTextureAtlas()));
        }

        this.getMap().setBlockAtXY(27, 7, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(26, 7, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(25, 7, new Brick(this.getBlocksTextureAtlas()));


        this.getMap().setBlockAtXY(18, 9, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(20, 9, new Brick(this.getBlocksTextureAtlas()));

        this.getMap().setBlockAtXY(22, 13, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(23, 13, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(24, 13, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(25, 13, new Brick(this.getBlocksTextureAtlas()));

        this.getMap().setBlockAtXY(26, 17, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(27, 17, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(28, 17, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(29, 17, new Brick(this.getBlocksTextureAtlas()));

        for(int i = 0; i < 14; i++)
            this.getMap().setBlockAtXY(56 + i, 0, new Stone(this.getBlocksTextureAtlas()));

        for(int i = 0; i < 5; i++)
            this.getMap().setBlockAtXY(30 + i, 0, new Stone(this.getBlocksTextureAtlas()));

        this.getMap().setBlockAtXY(56, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(66, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(67, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(68, 1, new Stone(this.getBlocksTextureAtlas()));
        //this.addEnemy(new Mushroom(this.getEnemiesTextureAtlas(), 60, 1));
        //this.addEnemy(new Mushroom(this.getEnemiesTextureAtlas(), 58, 1));
        this.addEnemy(new Smile(this.getEnemiesTextureAtlas(), 63, 1));

        this.getMap().setBlockAtXY(50, 4, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(49, 4, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(48, 4, new Brick(this.getBlocksTextureAtlas()));

        this.getMap().setBlockAtXY(43, 6, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(42, 6, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(41, 6, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(40, 6, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(39, 6, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(38, 6, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(38, 7, new Brick(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(43, 7, new Brick(this.getBlocksTextureAtlas()));
        this.addEnemy(new Mushroom(this.getEnemiesTextureAtlas(), 39, 7));
        this.addEnemy(new Mushroom(this.getEnemiesTextureAtlas(), 42, 7));

        this.getMap().setBlockAtXY(32, 1, new Stone(this.getBlocksTextureAtlas()));
        this.getMap().setBlockAtXY(32, 2, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(32, 3, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(32, 4, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(32, 5, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(32, 6, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(32, 7, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(32, 8, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(32, 9, new Flag(this.getBlocksTextureAtlas(), false));
        this.getMap().setBlockAtXY(32, 10, new Flag(this.getBlocksTextureAtlas(), true));
    }

}
