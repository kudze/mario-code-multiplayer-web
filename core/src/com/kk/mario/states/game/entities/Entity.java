package com.kk.mario.states.game.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.kk.mario.MarioGame;
import com.kk.mario.states.game.LevelState;
import com.kk.mario.states.game.map.Block;
import com.kk.mario.states.game.map.Map;

public abstract class Entity {
    public enum Direction {
        LEFT, RIGHT
    }

    public enum HitDirection {
        UP, DOWN, LEFT, RIGHT
    }

    private int width;
    private int height;
    private Vector2 force;
    private Vector2 position;
    private Direction direction;

    public Entity(int width, int height, int spawnX, int spawnY) {
        this.width = width;
        this.height = height;

        this.position = new Vector2(spawnX * 16, spawnY * 16);
        this.force = new Vector2();
        this.direction = Direction.RIGHT;
    }

    public void update() {
        int blockX = (int) Math.round(this.position.x / 16);
        int blockY = (int) Math.round(this.position.y / 16);

        if (this.force.x < 0 && !this.canGoLeft()) {
            this.force.x = 0;
            this.position.x = ((float) Math.ceil(this.position.x / 16) * 16);

            this.collidedWithBlock(this.getMap().getBlockAtXY(blockX, blockY), HitDirection.LEFT);
        }

        if (this.force.x > 0 && !this.canGoRight()) {
            this.force.x = 0;
            this.position.x = ((float) Math.floor(this.position.x / 16) * 16);

            this.collidedWithBlock(this.getMap().getBlockAtXY(blockX + 1, blockY), HitDirection.RIGHT);
        }

        if (this.force.x != 0) {
            this.position.x += this.force.x * Gdx.graphics.getDeltaTime();
        }

        if(this.canGoDown()) {
            this.force.y = Math.max(this.force.y - (Gdx.graphics.getDeltaTime() * 200), -150);
        } else if(this.force.y < 0) {
            this.force.y = 0;
            this.position.y = ((float) Math.ceil(this.position.y / 16) * 16);
            this.collidedWithBlock(this.getMap().getBlockAtXY(blockX, blockY), HitDirection.DOWN);
        }

        if(this.force.y > 0 && !this.canGoUp()) {
            this.force.y = 0;
            this.position.y = ((float) Math.floor(this.position.y / 16) * 16);
            this.collidedWithBlock(this.getMap().getBlockAtXY(blockX, blockY + 1), HitDirection.UP);
        }

        if (this.force.y != 0)
            this.position.y += this.force.y * Gdx.graphics.getDeltaTime();
    }

    public abstract void render(Batch batch);
    public abstract void dispose();
    protected abstract void collidedWithBlock(Block block, HitDirection direction);

    protected void renderSprite(Batch batch, Sprite sprite) {
        if (this.getDirection() == Direction.LEFT)
            sprite.setFlip(true, false);
        else
            sprite.setFlip(false, false);

        Vector2 position = this.getPosition();
        sprite.setPosition(position.x, position.y);
        sprite.draw(batch);
    }

    public boolean canGoDown() {
        float ypos = this.position.y - 0.001f;
        int y1 = (int) Math.floor(ypos / 16);
        int y2 = (int) Math.ceil(ypos / 16);
        int minY = Math.min(y1, y2);

        if (minY < 0)
            return true;

        float realX = this.position.x / 16;
        int x1 = (int) Math.floor(this.position.x / 16);
        int x2 = (int) Math.ceil(this.position.x / 16);

        if (Math.abs(realX - x1) < 0.2f)
            x2 = x1;

        if (Math.abs(x2 - realX) < 0.2f)
            x1 = x2;

        return !this.getMap().isBlockAtXYWithCollision(x1, minY) && !this.getMap().isBlockAtXYWithCollision(x2, minY);
    }

    public boolean canGoUp() {
        float ypos = this.position.y + 0.001f;
        int y1 = (int) Math.floor(ypos / 16);
        int y2 = (int) Math.ceil(ypos / 16);
        int maxY = Math.max(y1, y2);

        if (maxY < 0)
            return true;

        float realX = this.position.x / 16;
        int x1 = (int) Math.floor(this.position.x / 16);
        int x2 = (int) Math.ceil(this.position.x / 16);

        if (Math.abs(realX - x1) < 0.2f)
            x2 = x1;

        if (Math.abs(x2 - realX) < 0.2f)
            x1 = x2;

        return !this.getMap().isBlockAtXYWithCollision(x1, maxY) && !this.getMap().isBlockAtXYWithCollision(x2, maxY);
    }

    public boolean canGoLeft() {
        float xpos = this.position.x - 0.001f;

        int x1 = (int) Math.floor(xpos / 16);
        int x2 = (int) Math.ceil(xpos / 16);
        int minX = Math.min(x1, x2);

        if (minX < 0)
            return true;

        float realY = this.position.y / 16;
        int y1 = (int) Math.floor((this.position.y) / 16);
        int y2 = (int) Math.ceil((this.position.y) / 16);

        if (Math.abs(realY - y1) < 0.2f)
            y2 = y1;

        if (Math.abs(y2 - realY) < 0.2f)
            y1 = y2;

        return !this.getMap().isBlockAtXYWithCollision(minX, y1) && !this.getMap().isBlockAtXYWithCollision(minX, y2);
    }

    public boolean canGoRight() {
        float xpos = this.position.x + 0.001f;

        int x1 = (int) Math.ceil(xpos / 16);
        int x2 = (int) Math.floor(xpos / 16);
        int maxX = Math.max(x1, x2);

        if (maxX < 0)
            return true;

        float realY = this.position.y / 16;
        int y1 = (int) Math.floor((this.position.y) / 16);
        int y2 = (int) Math.ceil((this.position.y) / 16);

        if (Math.abs(realY - y1) < 0.2f)
            y2 = y1;

        if (Math.abs(y2 - realY) < 0.2f)
            y1 = y2;

        return !this.getMap().isBlockAtXYWithCollision(maxX, y1) && !this.getMap().isBlockAtXYWithCollision(maxX, y2);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Vector2 getForce() {
        return force;
    }

    public void setForce(Vector2 force) {
        this.force = force;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void setPosition(Vector2 position) {
        this.position = position;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    protected Map getMap() {
        LevelState levelState = (LevelState) MarioGame.getInstance().getGameStateManager().getActiveGameState();

        return levelState.getMap();
    }
}
