package com.kk.mario.states.game.entities.mario;

import com.badlogic.gdx.math.Vector2;
import com.kk.mario.states.game.LevelState;
import com.kk.mario.states.game.code.API;

public class MarioInputFaker implements MarioInputInterface {
    private boolean left = false, right = false, jump = false;
    private Mario mario;

    public MarioInputFaker(Mario mario) {
        this.mario = mario;
    }

    @Override
    public boolean isPressingLeft() {
        Mario mario = this.getMario();
        API api = mario.getLevel().getSimulator().getApi();

        this.updateAPIPoint();

        if (api.hasStabilizedPoint() && (mario.getPosition().x > (api.getStabilizedPointX() * 16.f))) {
            int random = (int) Math.floor(Math.random() * 2);
            return random == 0;
        }

        return left && !api.hasStabilizedPoint();
    }

    @Override
    public boolean isPressingRight() {
        Mario mario = this.getMario();
        API api = mario.getLevel().getSimulator().getApi();

        this.updateAPIPoint();

        if (api.hasStabilizedPoint() && (mario.getPosition().x < (api.getStabilizedPointX() * 16.f))) {
            int random = (int) Math.floor(Math.random() * 2);
            return random != 0;
        }

        return right && !api.hasStabilizedPoint();
    }

    @Override
    public boolean isPressingJump() {
        return jump;
    }

    public void setLeftPressed(boolean left) {
        this.left = left;
    }

    public void setRightPressed(boolean right) {
        this.right = right;
    }

    public void setJumpPressed(boolean jump) {
        this.jump = jump;
    }

    public Mario getMario() {
        return this.mario;
    }

    void setMario(Mario mario) {
        this.mario = mario;
    }

    private void updateAPIPoint() {
        Mario mario = this.getMario();
        API api = mario.getLevel().getSimulator().getApi();

        if(!api.hasStabilizedPoint())
            return;

        float dist = Math.abs((api.getStabilizedPointX() * 16.f) - mario.getPosition().x);
        if(dist <= 2.f && Math.abs(mario.getForce().x) <= 2.f)
        {
            api.stabilizeEnd();
        }
    }
}
